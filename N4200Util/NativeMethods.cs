﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace N4200Util
{
    class NativeMethods
    {
        private const uint N4200GPIO_DEVICE_TYPE = 0xE56DD526;

        private readonly SafeHandle _deviceHandle;

        public NativeMethods()
        {
            _deviceHandle = CreateFile(@"\\.\N4200GPIO", FileAccess.ReadWrite, FileShare.ReadWrite, IntPtr.Zero, FileMode.Open, 0,
                                 IntPtr.Zero);
            if (_deviceHandle.IsInvalid)
            {
                var exc = new Win32Exception();
                Console.WriteLine("Failed to open device handle: " + exc.Message);
                throw exc;
            }

        }

        [Flags]
        private enum IoMethod : uint
        {
            Buffered = 0,
            InDirect = 1,
            OutDirect = 2,
            Neither = 3
        }

        private enum Ioctl : uint
        {
            N4200GpioSetLedStatus = (N4200GPIO_DEVICE_TYPE << 16 | (0x0802 << 2) | IoMethod.Buffered | (FileAccess.ReadWrite << 14))
        }

        public enum Led : byte
        {
            Sata0Healthy = 0,
            Sata1Healthy = 1,
            Sata2Healthy = 2,
            Sata3Healthy = 3,
            Sata0Fail = 4,
            Sata1Fail = 5,
            Sata2Fail = 6,
            Sata3Fail = 7,
            UsbHealthy = 8,
            UsbFail = 9,
        };
        public void SetLed(Led led, bool status)
        {
            unsafe
            {
                uint bytesReturned = 0;
                IntPtr pBuffer = Marshal.AllocHGlobal(2);
                byte* buffer = (byte*)pBuffer.ToPointer();
                buffer[0] = (byte)led;
                buffer[1] = status ? (byte)1 : (byte)0;

                Console.WriteLine("request: " + buffer[0] + " " + buffer[1]);

                bool res = DeviceIoControl(_deviceHandle.DangerousGetHandle(), (uint)Ioctl.N4200GpioSetLedStatus, pBuffer, 2, IntPtr.Zero, 0,
                                           out bytesReturned, IntPtr.Zero);
                if (!res)
                {
                    var exc = new Win32Exception();
                    Console.WriteLine("IOCTL failed: " + exc.Message);
                }

                Marshal.FreeHGlobal(pBuffer);
            }
        }


        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern SafeFileHandle CreateFile(
            string lpFileName,
            [MarshalAs(UnmanagedType.U4)] FileAccess dwDesiredAccess,
            [MarshalAs(UnmanagedType.U4)] FileShare dwShareMode,
            IntPtr lpSecurityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode dwCreationDisposition,
            [MarshalAs(UnmanagedType.U4)] FileAttributes dwFlagsAndAttributes,
            IntPtr hTemplateFile);

        [DllImport("kernel32.dll", ExactSpelling = true, SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool DeviceIoControl(IntPtr hDevice, uint dwIoControlCode,
        IntPtr lpInBuffer, uint nInBufferSize,
        IntPtr lpOutBuffer, uint nOutBufferSize,
        out uint lpBytesReturned, IntPtr lpOverlapped);

    }
}
