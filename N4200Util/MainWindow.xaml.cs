﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N4200Util
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly NativeMethods _deviceBackend;

        public MainWindow()
        {
            InitializeComponent();
            _deviceBackend = new NativeMethods();
        }

        private void SATA0Green_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata0Healthy, SATA0Green.IsChecked.GetValueOrDefault());
        }
        private void SATA0Red_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata0Fail, SATA0Red.IsChecked.GetValueOrDefault());
        }
        private void SATA1Green_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata1Healthy, SATA1Green.IsChecked.GetValueOrDefault());
        }
        private void SATA1Red_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata1Fail, SATA1Red.IsChecked.GetValueOrDefault());
        }
        private void SATA2Green_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata2Healthy, SATA2Green.IsChecked.GetValueOrDefault());
        }
        private void SATA2Red_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata2Fail, SATA2Red.IsChecked.GetValueOrDefault());
        }
        private void SATA3Green_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata3Healthy, SATA3Green.IsChecked.GetValueOrDefault());
        }
        private void SATA3Red_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.Sata3Fail, SATA3Red.IsChecked.GetValueOrDefault());
        }
        private void USBGreen_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.UsbHealthy, USBGreen.IsChecked.GetValueOrDefault());
        }
        private void USBRed_Click(object sender, RoutedEventArgs e)
        {
            _deviceBackend.SetLed(NativeMethods.Led.UsbFail, USBRed.IsChecked.GetValueOrDefault());
        }
    }
}
