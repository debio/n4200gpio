﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace N4200Util
{
    class RegistryInterop
    {
        private RegistryKey _regKey;
        public RegistryInterop(string registryPath)
        {
            var baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
            _regKey = baseKey.OpenSubKey(registryPath + "\\Parameters", true);
        }

        const uint WOL0 = 1u;
        const uint WOL1 = 2u;

        public bool WakeOnLAN0
        {
            get
            {
                return (((UInt32)_regKey.GetValue("WakeOnLAN")) & WOL0) == WOL0;
            }
            set
            {
                var val = (UInt32)_regKey.GetValue("WakeOnLAN");
                if (value)
                    val |= WOL0;
                else
                    val &= ~WOL0;
                _regKey.SetValue("WakeOnLAN", val);
            }
        }

        public bool WakeOnLAN1
        {
            get
            {
                return (((UInt32)_regKey.GetValue("WakeOnLAN")) & WOL1) == WOL1;
            }
            set
            {
                var val = (UInt32)_regKey.GetValue("WakeOnLAN");
                if (value)
                    val |= WOL1;
                else
                    val &= ~WOL1;
                _regKey.SetValue("WakeOnLAN", val);
            }
        }

        public enum PowerFailureBehavior : uint
        {
            StayOn = 1u,
            StayOff = 0u
        };

        public PowerFailureBehavior OnPowerFailure
        {
            get
            {
                return PowerFailureBehavior.StayOn;
            }
            set
            {

            }
        }
    }
}
