N4200GPIO
=========

Windows Drivers for Built-in Peripherals on the Thecus N4200 NAS Box

Overall Structure
=================

- Misc contains dsdt-patched.asl, which is the source for an ACPI DSDT table that can be used with the N4200Eco.
- N4200GPIO is a device driver that controls the GPIO controller on the N4200's ICH9 chipset (device exposed to Windows by the patched DSDT table) to control the front panel LEDs on the device.
- N4200Util is a C# sample app that uses system calls to exercise the N4200GPIO code.

What Works
==========
- Front Panel LEDs
- Wake On LAN

What Needs Work
===============
- Behavior on Power Restoration

The Thecus kernel module on Linux allows the user to determine whether the device will boot or stay off when power is restored after an outage.

- Hard Drive Filter Driver

Now that the front panel LEDs are controlled by the N4200GPIO driver, the next step will be to write a simple filter driver for the SATA ports to toggle the LEDs properly.

- I2C Peripherals

The N4200ECO may have LED dimmers and definitely has a microcontroller to drive the front panel OLED/buttons on the SMBus. The vendor Linux drivers treat these devices as I2C devices rather than SMBus devices, so it may be possible to link the SMBus controller into the Win8/Server2012 Simple Peripheral Bus framework and work from there. Initially, though, we need a solid determination of what devices are present at what addresses and which ones are safe to drive from the OS.

Things To Be Aware Of
=====================
GPIO and SMBus/I2C are very much unsupported in non-SoC Windows. They are officially owned by the BIOS, although support was added in Win8/Server2012 to support Windows on simpler devices, like tablets. What these drivers do is safe on the machine they're intended for, as this machine was designed for the OS to drive GPIO and certain I2C peripherals and there is no conflict with the system firmware. In general, however, these drivers are not safe for use on any other device and are generally contrary to Windows driver development best practices.
