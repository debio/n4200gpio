/*++

Module Name:

device.h

Abstract:

This file contains the device definitions.

Environment:

Kernel-mode Driver Framework

--*/

#include "public.h"

extern "C" {
	//
	// Function to initialize the device and its callbacks
	//
	NTSTATUS
		N4200GPIOCreateDevice(
		_Inout_ PWDFDEVICE_INIT DeviceInit
		);

	NTSTATUS
		N4200D0Entry(
		_In_
		WDFDEVICE Device,
		_In_
		WDF_POWER_DEVICE_STATE TargetState
		);

}