#include "Driver.h"
#include "N4200.h"
// GPIO definitions

using namespace GPIO;

const int GPIO::NUM_PINS = 19;

PinSettings GPIO::AllPinSettings[NUM_PINS] = {
	{Pins::SATA0_HEALTHY, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA0 Green LED (inverted)"},
	{Pins::SATA1_HEALTHY, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA1 Green LED (inverted)"},
	{Pins::SATA2_HEALTHY, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA2 Green LED (inverted)"},
	{Pins::SATA3_HEALTHY, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA3 Green LED (inverted)"},
	{Pins::SATA0_FAIL, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA0 Red LED (inverted)"},
	{Pins::SATA1_FAIL, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA1 Red LED (inverted)"},
	{Pins::SATA2_FAIL, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA2 Red LED (inverted)"},
	{Pins::SATA3_FAIL, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "SATA3 Red LED (inverted)"},
	{Pins::USB_HEALTHY, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "USB Green LED (inverted)"},
	{Pins::USB_FAIL, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "USB Red LED (inverted)"},
	{Pins::RESET_OLED, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "Reset OLED screen"},
	{Pins::BACKLIGHT_ENABLE, PinRole::OUTPUT, PinDefaultOutput::ZERO, PinInverted::NORMAL, "LED backlight (inverted)"},
	{Pins::BUS_ID, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::NORMAL, "BUS_ID - what does that mean?"},
	{Pins::WAKE_ON_LAN_0, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::INVERT, "first WakeOnLAN interrupt line"},
	{Pins::WAKE_ON_LAN_1, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::INVERT, "second WakeOnLAN interrupt line"},
	{Pins::BOARD_ID_0, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::NORMAL, "board ID line - always 1"},
	{Pins::BOARD_ID_1, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::NORMAL, "board ID line - always 0"},
	{Pins::POWER_CONNECTED, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::NORMAL, "power connected - always 1 when we can read it"},
	{Pins::HAS_BATTERY, PinRole::INPUT, PinDefaultOutput::NONE, PinInverted::NORMAL, "battery present - always 0"},
};

void GPIO::InitializePin(PortSettings& settings, PinSettings& pinSettings)
{
	SetPin(settings.UseSelect, pinSettings.Index);
	if(pinSettings.Role == PinRole::OUTPUT)
	{
		ClearPin(settings.UseRole, pinSettings.Index);
		if(pinSettings.DefaultOutput == PinDefaultOutput::ONE)
			SetPin(settings.PinLevel, pinSettings.Index);
		else if(pinSettings.DefaultOutput == PinDefaultOutput::ZERO)
			ClearPin(settings.PinLevel, pinSettings.Index);
	}
	else
	{
		SetPin(settings.UseRole, pinSettings.Index);
		if(pinSettings.Inverted == PinInverted::INVERT)
			SetPin(settings.PinInvert, pinSettings.Index);
		else
			ClearPin(settings.PinInvert, pinSettings.Index);
	}
}

void GPIO::SetPin(Port &port, Pin pin)
{
	port |= (1 << (pin % 32));
}
void GPIO::ClearPin(Port &port, Pin pin)
{
	port &= ~(1 << (pin % 32));
}
