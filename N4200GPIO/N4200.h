#pragma once

typedef unsigned __int32 Port;
typedef unsigned __int32 Pin;

// N4200 Eco GPIO Pin Definitions

namespace GPIO
{
	namespace Pins {
		enum Index : unsigned __int32 {
			SATA0_HEALTHY = 0,
			SATA1_HEALTHY = 2,
			SATA2_HEALTHY = 3,
			SATA3_HEALTHY = 4,
			//SATA4_HEALTHY = 5, // no SATA4 LED in this device
			POWER_CONNECTED = 9,
			BUS_ID = 10, // what is this?
			WAKE_ON_LAN_0 = 12,
			WAKE_ON_LAN_1 = 13,
			BOARD_ID_0 = 18, // always 1 on this board
			//SATA5_HEALTHY = 22, // no SATA5 LED in this device
			USB_HEALTHY = 24,
			HAS_BATTERY = 25, // always 0 on this board
			//FILE_COPY_BUTTON = 26 // no copy button in this device (there's also a large amount of configuration required to use this pin because Intel normally uses it for something else?)
			BOARD_ID_1 = 27, // always 0 on this board
			//CHARGE_BATTERY = 28, // no battery to charge in this device
			SATA0_FAIL = 32,
			SATA1_FAIL = 33,
			BACKLIGHT_ENABLE = 34,
			USB_FAIL = 35,
			SATA2_FAIL = 38,
			SATA3_FAIL = 39,
			//SATA4_FAIL = 48, // no SATA4 LED in this device
			//SATA5_FAIL = 55, // no SATA5 LED in this device
			RESET_OLED = 56
		};
	}

	namespace PinRole
	{
		enum Role {
			INPUT,
			OUTPUT
		};
	}

	namespace PinDefaultOutput {
		enum DefaultOutput {
			NONE = -1,
			ZERO = 0,
			ONE = 1
		};
	}

	namespace PinInverted {
		enum PinInverted {
			NORMAL = 0, 
			INVERT = 1
		};
	}

	struct PinSettings {
		Pins::Index Index;
		PinRole::Role Role;
		PinDefaultOutput::DefaultOutput DefaultOutput;
		PinInverted::PinInverted Inverted;
		const char * Name;
	};

	struct PortSettings {
		Port UseSelect;
		Port UseRole;
		Port PinLevel;
		Port PinInvert;
	};

	extern const int NUM_PINS;
	extern PinSettings AllPinSettings[];

	void InitializePin(PortSettings& settings, PinSettings& pinSettings);

	void SetPin(Port &port, Pin pin);
	void ClearPin(Port &port, Pin pin);
}

namespace Settings {
	struct BoardSettings;
}

class N4200Eco
{
private:
	ULONG gpioBase;

	N4200Eco(); // never run due to how the context is allocated

public:
	void Preinit();
	NTSTATUS Init(ULONG gpioPortBase);
	NTSTATUS SetOutput(GPIO::Pins::Index pin, BYTE status);
};

typedef N4200Eco DEVICE_CONTEXT;
typedef DEVICE_CONTEXT *PDEVICE_CONTEXT;

//
// This macro will generate an inline function called DeviceGetContext
// which will be used to get a pointer to the device context memory
// in a type safe manner.
//
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_CONTEXT, DeviceGetContext)