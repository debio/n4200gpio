#include "Driver.h"
#include "N4200.h"

namespace GPIO_OFF {
	enum GPIO_OFFSETS : unsigned __int32 {
		GPIO_USE_SEL = 0x00,
		GP_IO_SEL = 0x04,
		GP_LVL = 0x0c,
		GPO_BLINK = 0x18,
		GPI_INV = 0x2c, 
		GPIO_USE_SEL2 = 0x30,
		GP_IO_SEL2 = 0x34,
		GP_LVL2 = 0x38
	};
}

void N4200Eco::Preinit()
{
	gpioBase = (ULONG)-1;
}

NTSTATUS N4200Eco::Init(ULONG gpioPortBase)
{
	NTSTATUS status = STATUS_SUCCESS;
	gpioBase = gpioPortBase;

#pragma region internal GPIO initialization
	{
		GPIO::PortSettings p1, p2;

		// read all the registers
		p1.UseSelect = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPIO_USE_SEL));
		p1.UseRole = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_IO_SEL));
		p1.PinInvert = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPI_INV));
		p1.PinLevel = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL));
		DbgPrint("GPIO_USE_SEL initially: 0x%08x\n", p1.UseSelect);
		DbgPrint("GP_IO_SEL initially: 0x%08x\n", p1.UseRole);
		DbgPrint("GPI_INV initially: 0x%08x\n", p1.PinInvert);
		DbgPrint("GP_LVL initially: 0x%08x\n", p1.PinLevel);

		p2.UseSelect = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPIO_USE_SEL2));
		p2.UseRole = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_IO_SEL2));
		p2.PinInvert = 0; // p2 has no inversion register
		p2.PinLevel = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL2));
		DbgPrint("GPIO_USE_SEL2 initially: 0x%08x\n", p2.UseSelect);
		DbgPrint("GP_IO_SEL2 initially: 0x%08x\n", p2.UseRole);
		DbgPrint("p2.PinInvert initially: 0x%08x\n", p2.PinInvert);
		DbgPrint("GP_LVL2 initially: 0x%08x\n", p2.PinLevel);  

		// init pins
		for(int i = 0; i < GPIO::NUM_PINS; i++)
		{
			GPIO::PinSettings &curPin = GPIO::AllPinSettings[i];
			if(curPin.Index < 32)
				GPIO::InitializePin(p1, curPin);
			else
				GPIO::InitializePin(p2, curPin);
		}

		// write all the registers
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPIO_USE_SEL), p1.UseSelect);
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_IO_SEL), p1.UseRole);
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPI_INV), p1.PinInvert);
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL), p1.PinLevel);
		DbgPrint("GPIO_USE_SEL set to: 0x%08x\n", p1.UseSelect);
		DbgPrint("GP_IO_SEL set to: 0x%08x\n", p1.UseRole);
		DbgPrint("GPI_INV set to: 0x%08x\n", p1.PinInvert);
		DbgPrint("GP_LVL set to: 0x%08x\n", p1.PinLevel);

		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPIO_USE_SEL2), p2.UseSelect);
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_IO_SEL2), p2.UseRole);
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL2), p2.PinLevel);
		DbgPrint("GPIO_USE_SEL2 set to: 0x%08x\n", p2.UseSelect);
		DbgPrint("GP_IO_SEL2 set to: 0x%08x\n", p2.UseRole);
		DbgPrint("p2.PinInvert would be set to: 0x%08x\n", p2.PinInvert);
		DbgPrint("GP_LVL2 set to: 0x%08x\n", p2.PinLevel);

		ASSERT(p2.PinInvert == 0); // if this is nonzero, we're asking the hardware to invert a pin it can't invert

		// check
		p1.UseSelect = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPIO_USE_SEL));
		p1.UseRole = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_IO_SEL));
		p1.PinInvert = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPI_INV));
		p1.PinLevel = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL));
		DbgPrint("GPIO_USE_SEL now: 0x%08x\n", p1.UseSelect);
		DbgPrint("GP_IO_SEL now: 0x%08x\n", p1.UseRole);
		DbgPrint("GPI_INV now: 0x%08x\n", p1.PinInvert);
		DbgPrint("GP_LVL now: 0x%08x\n", p1.PinLevel);  

		p2.UseSelect = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GPIO_USE_SEL2));
		p2.UseRole = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_IO_SEL2));
		p2.PinLevel = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL2));
		DbgPrint("GPIO_USE_SEL2 now: 0x%08x\n", p2.UseSelect);
		DbgPrint("GP_IO_SEL2 now: 0x%08x\n", p2.UseRole);
		DbgPrint("GP_LVL2 now: 0x%08x\n", p2.PinLevel);
	}

	return status;
}

NTSTATUS N4200Eco::SetOutput(GPIO::Pins::Index pin, BYTE set)
{
	NTSTATUS status = STATUS_SUCCESS;

	if(gpioBase == -1)
	{
		status = STATUS_DEVICE_NOT_READY;
		goto cleanup;
	}

	// make sure pin is valid
	int i = 0;
	for(i = 0; i < GPIO::NUM_PINS; i++)
	{
		if(GPIO::AllPinSettings[i].Index == pin)
		{
			if(GPIO::AllPinSettings[i].Role != GPIO::PinRole::OUTPUT)
			{
				status = STATUS_INVALID_DEVICE_REQUEST;
				goto cleanup;
			}
			else
				break;
		}
	}
	if(i == GPIO::NUM_PINS)
	{
		status = STATUS_INVALID_DEVICE_REQUEST;
		goto cleanup;
	}

	// get the level register
	Port level = 0;
	if(pin < 32)
		level = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL));
	else
		level = READ_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL2));
	// twiddle the pin
	if(set == 0)
		GPIO::ClearPin(level, pin);
	else
		GPIO::SetPin(level, pin);
	// write back
	if(pin < 32)
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL), level);
	else
		WRITE_PORT_ULONG((PULONG) (gpioBase + GPIO_OFF::GP_LVL2), level);

cleanup:
	return status;
}
