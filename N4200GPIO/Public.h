/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that app can find the device and talk to it.
//

DEFINE_GUID (GUID_DEVINTERFACE_N4200GPIO,
    0x03ffdf9a,0x7c50,0x48fe,0x81,0x3a,0x3a,0x4a,0x98,0xf4,0x9f,0x9e);
// {03ffdf9a-7c50-48fe-813a-3a4a98f49f9e}

#define N4200GPIO_DEVICE_TYPE 0xE56DD526 // generated randomly

#define IOCTL_N4200GPIO_GET_REGISTRY_PATH \
	CTL_CODE(N4200GPIO_DEVICE_TYPE, 0x801, METHOD_BUFFERED, \
        FILE_READ_DATA | FILE_WRITE_DATA)
#define IOCTL_N4200GPIO_SET_LED_STATUS \
	CTL_CODE(N4200GPIO_DEVICE_TYPE, 0x802, METHOD_BUFFERED, \
        FILE_READ_DATA | FILE_WRITE_DATA)

enum LED_ID : unsigned __int8
{
	LED_SATA0_HEALTHY = 0,
	LED_SATA1_HEALTHY = 1,
	LED_SATA2_HEALTHY = 2,
	LED_SATA3_HEALTHY = 3,
	LED_SATA0_FAIL = 4,
	LED_SATA1_FAIL = 5,
	LED_SATA2_FAIL = 6,
	LED_SATA3_FAIL = 7,
	LED_USB_HEALTHY = 8,
	LED_USB_FAIL = 9,
	LED_MAX = 10
};

#define IOCTL_N4200GPIO_RESET_OLED \
	CTL_CODE(N4200GPIO_DEVICE_TYPE, 0x803, METHOD_BUFFERED, \
        FILE_READ_DATA | FILE_WRITE_DATA)

namespace Settings {
	namespace WakeOnLAN {
		enum WakeOnLAN : ULONG {
			WAKE_ON_LAN_0_ENABLE = 1<<0,
			WAKE_ON_LAN_1_ENABLE = 1<<1
		};
	}

	namespace PowerFailure {
		enum PowerFailure : ULONG {
			STAY_OFF = 0,
			STAY_ON = 1
		};
	}
}


#define N4200_DOS_PATH L"\\\\.\\N4200GPIO"